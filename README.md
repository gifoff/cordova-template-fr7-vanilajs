# Cordova Template for framework7

Creating a cordova project from a template
```
$ cordova create hello com.example.hello HelloWorld --template <npm-package-name | <git-remote-url | local-path>
```

After succesfully using a template to create your project, you'll want to indicate the platforms that you intend to target with your app. Go into your project folder and [add platforms](http://cordova.apache.org/docs/en/latest/guide/cli/index.html#add-platforms).