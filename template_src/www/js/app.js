var $$ = Dom7;
var app = new Framework7({
        material:true,
        precompileTemplates: true,
        swipePanel: 'left',
        swipePanelActiveArea: 30,
    });
var mainView = app.addView('.view-main', {});

// blank.html
app.onPageInit('blank',function(page){
	app.alert('modal from blank.html');
});