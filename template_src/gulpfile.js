var gulp = require('gulp');
var rigger = require('gulp-rigger');

gulp.task('rigger', function () {
	gulp.src('js-rigger/app.js')
		.pipe(rigger())
		.pipe(gulp.dest('www/js/'));
});

gulp.task('fr7:css',function(){
	return gulp.src(['bower_components/framework7/dist/css/framework7.material.min.css'
					 ,'bower_components/framework7/dist/css/framework7.material.colors.min.css'])
		.pipe(gulp.dest('www/css'))
});

gulp.task('fr7:js',function(){
	return gulp.src('bower_components/framework7/dist/js/framework7.min.js')
		.pipe(gulp.dest('www/js'))
});
gulp.task('fr7',['fr7:css','fr7:js']);

gulp.task('default',function(){
	gulp.watch('js-rigger/*.js',['rigger']);
});

gulp.task('build',['fr7:css','fr7:js','rigger']);